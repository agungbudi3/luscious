# luscious

Luscious is food recipe collection application powered by [TheMealDB](https://www.themealdb.com). 
You can explore recipes by category, or you can simply search for the meal name. 
Luscious Save recipe you've viewed, so can easily back to that recipe.

## APK

[luscious.apk](readme_assets/apk/Luscious.apk)

## Screenshoot

<img src="readme_assets/screenshoot/Screenshot_1624049031.png" alt="home1" width="200"/>
<img src="readme_assets/screenshoot/Screenshot_1624049037.png" alt="home2" width="200"/>
<img src="readme_assets/screenshoot/Screenshot_1624049050.png" alt="category1" width="200"/>
<img src="readme_assets/screenshoot/Screenshot_1624049057.png" alt="category2" width="200"/>
<img src="readme_assets/screenshoot/Screenshot_1624049064.png" alt="meal1" width="200"/>
<img src="readme_assets/screenshoot/Screenshot_1624049064.png" alt="meal2" width="200"/>
<img src="readme_assets/screenshoot/Screenshot_1624049074.png" alt="meal3" width="200"/>
<img src="readme_assets/screenshoot/Screenshot_1624049079.png" alt="meal4" width="200"/>
<img src="readme_assets/screenshoot/Screenshot_1624049114.png" alt="search" width="200"/>
<img src="readme_assets/screenshoot/Screenshot_1624049128.png" alt="about" width="200"/>