import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:hydrated_bloc/hydrated_bloc.dart';
import 'package:luscious/untils/constants.dart';
import 'package:luscious/untils/network/endpoints.dart';
import 'package:luscious/untils/providers.dart';
import 'package:luscious/untils/routes.dart';
import 'package:path_provider/path_provider.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  HydratedBloc.storage = await HydratedStorage.build(
    storageDirectory: await getApplicationDocumentsDirectory(),
  );
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return RepositoryProvider(
      create: (_) => Endpoints(),
      child: MultiBlocProvider(
        providers: blocProviders,
        child: MaterialApp(
          title: 'Luscious',
          theme: ThemeData(
            primarySwatch: kPrimarySwatch,
            scaffoldBackgroundColor: kPrimaryColor,
          ),
          initialRoute: homeRoute,
          routes: routes,
        ),
      ),
    );
  }
}
