import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:luscious/models/meal.dart';
import 'package:luscious/untils/network/endpoints.dart';
import 'package:luscious/untils/network/network_exceptions.dart';
import 'package:meta/meta.dart';

part 'detail_event.dart';

part 'detail_state.dart';

class DetailBloc extends Bloc<DetailEvent, DetailState> {
  DetailBloc(this.endpoints) : super(DetailInitial());
  final Endpoints endpoints;

  @override
  Stream<DetailState> mapEventToState(
    DetailEvent event,
  ) async* {
    if (event is OnLoadMeal) {
      yield DetailLoading();

      var res = await endpoints.getMealDetail(mealId: event.mealId);
      yield* res.when(
        success: (mealRes) async* {
          if (mealRes.meals != null && mealRes.meals.length > 0) {
            yield DetailSuccess(mealRes.meals[0]);
          }
        },
        failure: (NetworkExceptions error) async* {
          yield DetailError(NetworkExceptions.getErrorMessage(error));
        },
      );
    }
  }
}
