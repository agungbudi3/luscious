part of 'detail_bloc.dart';

@immutable
abstract class DetailState {}

class DetailInitial extends DetailState {}

class DetailLoading extends DetailState {}

class DetailSuccess extends DetailState {
  final Meal meal;

  DetailSuccess(this.meal);
}
class DetailError extends DetailState {
  final String message;

  DetailError(this.message);
}
