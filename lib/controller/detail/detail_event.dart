part of 'detail_bloc.dart';

@immutable
abstract class DetailEvent {}

class OnLoadMeal extends DetailEvent {
  final String mealId;

  OnLoadMeal(this.mealId);
}