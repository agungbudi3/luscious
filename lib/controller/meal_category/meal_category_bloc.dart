import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:luscious/models/meal.dart';
import 'package:luscious/untils/network/endpoints.dart';
import 'package:luscious/untils/network/network_exceptions.dart';
import 'package:meta/meta.dart';

part 'meal_category_event.dart';

part 'meal_category_state.dart';

class MealCategoryBloc extends Bloc<MealCategoryEvent, MealCategoryState> {
  MealCategoryBloc(this.endpoints) : super(MealCategoryState());
  final Endpoints endpoints;

  @override
  Stream<MealCategoryState> mapEventToState(
    MealCategoryEvent event,
  ) async* {
    if (event is OnLoadMealByCategory) {
      yield (state.copyWith(isLoading: true));

      var res = await endpoints.getMeals(category: event.category);
      yield* res.when(
        success: (mealRes) async* {
          List<Meal> meals = [];
          if(mealRes.meals != null){
            meals.addAll(mealRes.meals);
          }

          yield state.copyWith(
            meals: meals,
          );
        },
        failure: (NetworkExceptions error) async* {
          yield state.copyWith(
            isError: true,
            errorMessage: NetworkExceptions.getErrorMessage(error),
          );
        },
      );
    }
  }
}
