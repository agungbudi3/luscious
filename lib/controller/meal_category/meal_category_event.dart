part of 'meal_category_bloc.dart';

@immutable
abstract class MealCategoryEvent {}

class OnLoadMealByCategory extends MealCategoryEvent {
  final String category;

  OnLoadMealByCategory(this.category);
}
