part of 'meal_category_bloc.dart';

@immutable
class MealCategoryState {
  static const List<Meal> defaultMeal = [];
  final List<Meal> meals;
  final bool isLoading;
  final bool isError;
  final String errorMessage;

  MealCategoryState({
    this.meals = defaultMeal,
    this.isLoading = false,
    this.isError = false,
    this.errorMessage = "",
  });

  MealCategoryState copyWith({
    List<Meal> meals,
    bool isLoading,
    bool isError,
    String errorMessage,
  }) {
    return MealCategoryState(
      meals: meals ?? this.meals,
      isLoading: isLoading ?? false,
      isError: isError ?? false,
      errorMessage: errorMessage ?? "",
    );
  }
}
