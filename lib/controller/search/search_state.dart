part of 'search_bloc.dart';

@immutable
class SearchState {
  static const List<Meal> defaultMeal = [];
  final List<Meal> meals;
  final bool isSearch;
  final bool isLoading;
  final bool isError;
  final String errorMessage;
  final String searchQuery;

  SearchState({
    this.meals = defaultMeal,
    this.isSearch = false,
    this.isLoading = false,
    this.isError = false,
    this.errorMessage = "",
    this.searchQuery = "",
  });

  SearchState copyWith({
    List<Meal> meals,
    bool isSearch,
    bool isLoading,
    bool isError,
    String errorMessage,
    String searchQuery,
  }) {
    return SearchState(
      meals: meals ?? this.meals,
      isSearch: isSearch ?? this.isSearch,
      isLoading: isLoading ?? false,
      isError: isError ?? false,
      errorMessage: errorMessage ?? "",
      searchQuery: searchQuery ?? this.searchQuery,
    );
  }
}
