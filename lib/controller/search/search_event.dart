part of 'search_bloc.dart';

@immutable
abstract class SearchEvent {}

class OnSearchMeal extends SearchEvent{
  final String search;

  OnSearchMeal(this.search);
}

class OnSearchFocusChange extends SearchEvent{
  final bool hasFocus;

  OnSearchFocusChange(this.hasFocus);
}

