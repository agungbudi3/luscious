import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:luscious/models/meal.dart';
import 'package:luscious/untils/network/endpoints.dart';
import 'package:luscious/untils/network/network_exceptions.dart';
import 'package:meta/meta.dart';

part 'search_event.dart';

part 'search_state.dart';

class SearchBloc extends Bloc<SearchEvent, SearchState> {
  SearchBloc(this.endpoints) : super(SearchState());
  final Endpoints endpoints;

  @override
  Stream<SearchState> mapEventToState(
    SearchEvent event,
  ) async* {
    if (event is OnSearchFocusChange) {
      List<Meal> meals = state.meals;
      String searchQuery = state.searchQuery;
      if (!event.hasFocus) {
        meals = [];
        searchQuery = "";
      }
      yield state.copyWith(isSearch: event.hasFocus, meals: meals, searchQuery: searchQuery);
    } else if (event is OnSearchMeal) {
      yield (state.copyWith(isLoading: true, searchQuery: event.search));

      var res = await endpoints.searchMeals(search: event.search);
      yield* res.when(
        success: (mealRes) async* {
          List<Meal> meals = [];

          if (mealRes.meals != null) {
            meals.addAll(mealRes.meals);
          }

          yield state.copyWith(
            meals: meals,
          );
        },
        failure: (NetworkExceptions error) async* {
          yield state.copyWith(
            isError: true,
            errorMessage: NetworkExceptions.getErrorMessage(error),
          );
        },
      );
    }
  }
}
