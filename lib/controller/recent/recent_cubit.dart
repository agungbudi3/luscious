import 'package:bloc/bloc.dart';
import 'package:hydrated_bloc/hydrated_bloc.dart';
import 'package:luscious/models/meal.dart';
import 'package:meta/meta.dart';

part 'recent_state.dart';

class RecentCubit extends Cubit<RecentState> with HydratedMixin {
  RecentCubit() : super(RecentState([]));

  addMeal(Meal meal) {
    var recentmeal = state.recentMeal;
    recentmeal.removeWhere((element) => element.idMeal == meal.idMeal);
    recentmeal.insert(0, meal);
    if(recentmeal.length > 5){
      recentmeal.removeAt(5);
    }
    emit(state.copyWith(recentMeal: recentmeal));
  }

  @override
  RecentState fromJson(Map<String, dynamic> json) {
    return RecentState.fromMap(json);
  }

  @override
  Map<String, dynamic> toJson(RecentState state) {
    return state.toMap();
  }
}
