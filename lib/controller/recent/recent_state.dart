part of 'recent_cubit.dart';

@immutable
class RecentState {
  final List<Meal> recentMeal;

  RecentState(this.recentMeal);

  RecentState copyWith({
    List<Meal> recentMeal,
  }) {
    return RecentState(
      recentMeal ?? this.recentMeal,
    );
  }

  factory RecentState.fromMap(Map<String, dynamic> map) {
    List<Meal> recentMeal = [];
    if (map['recentMeal'] != null) {
      map['recentMeal'].forEach((v) {
        recentMeal.add(Meal.fromJson(v));
      });
    }
    return new RecentState(
      recentMeal,
    );
  }

  Map<String, dynamic> toMap() {
    // ignore: unnecessary_cast
    return {
      'recentMeal': this.recentMeal.map((v) => v.toJson()).toList(),
    } as Map<String, dynamic>;
  }
}
