part of 'category_bloc.dart';

@immutable
class CategoryState {
  static const List<Category> defaultCategory = [];
  final List<Category> categories;
  final bool isLoading;
  final bool isError;
  final String errorMessage;

  CategoryState({
    this.categories = defaultCategory,
    this.isLoading = false,
    this.isError = false,
    this.errorMessage = "",
  });

  CategoryState copyWith({
    List<Category> categories,
    bool isLoading,
    bool isError,
    String errorMessage,
  }) {
    return CategoryState(
      categories: categories ?? this.categories,
      isLoading: isLoading ?? false,
      isError: isError ?? false,
      errorMessage: errorMessage ?? "",
    );
  }

  factory CategoryState.fromMap(Map<String, dynamic> map) {
    List<Category> categories = [];
    if (map['categories'] != null) {
      map['categories'].forEach((v) {
        categories.add(Category.fromJson(v));
      });
    }
    return new CategoryState(
      categories: categories,
      isLoading: map['isLoading'] as bool,
      isError: map['isError'] as bool,
      errorMessage: map['errorMessage'] as String,
    );
  }

  Map<String, dynamic> toMap() {
    // ignore: unnecessary_cast
    return {
      'categories': this.categories.map((v) => v.toJson()).toList(),
      'isLoading': this.isLoading,
      'isError': this.isError,
      'errorMessage': this.errorMessage,
    } as Map<String, dynamic>;
  }
}
