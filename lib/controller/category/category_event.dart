part of 'category_bloc.dart';

@immutable
abstract class CategoryEvent {}

class OnLoadCategory extends CategoryEvent {}
