import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:hydrated_bloc/hydrated_bloc.dart';
import 'package:luscious/models/category.dart';
import 'package:luscious/untils/network/endpoints.dart';
import 'package:luscious/untils/network/network_exceptions.dart';
import 'package:meta/meta.dart';

part 'category_event.dart';

part 'category_state.dart';

class CategoryBloc extends Bloc<CategoryEvent, CategoryState> with HydratedMixin {
  CategoryBloc(this.endpoints) : super(CategoryState());
  final Endpoints endpoints;

  @override
  Stream<CategoryState> mapEventToState(
    CategoryEvent event,
  ) async* {
    if (event is OnLoadCategory) {
      yield (state.copyWith(isLoading: true));

      var res = await endpoints.getCategories();
      yield* res.when(
        success: (categoryRes) async* {
          List<Category> categories = [];
          categories.addAll(categoryRes.categories);

          yield state.copyWith(
            categories: categories,
          );
        },
        failure: (NetworkExceptions error) async* {
          yield state.copyWith(
            isError: true,
            errorMessage: NetworkExceptions.getErrorMessage(error),
          );
        },
      );
    }
  }

  @override
  CategoryState fromJson(Map<String, dynamic> json) {
    return CategoryState.fromMap(json);
  }

  @override
  Map<String, dynamic> toJson(CategoryState state) {
    return state.toMap();
  }
}
