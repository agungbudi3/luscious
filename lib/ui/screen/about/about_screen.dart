import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:luscious/ui/widgets/my_custom_appbar.dart';
import 'package:luscious/ui/widgets/section_title.dart';
import 'package:luscious/untils/constants.dart';
import 'package:url_launcher/url_launcher.dart';

class AboutScreen extends StatelessWidget {
  const AboutScreen({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final double paddingTop = MediaQuery.of(context).padding.top;
    return Scaffold(
      appBar: MyCustomAppBar(title: "About", padding: paddingTop),
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SizedBox(height: kDefaultPadding / 2),
            SectionAboutMe(),
            SectionAboutLuscious(),
            SectionAboutTechnology(),
            SizedBox(height: kDefaultPadding),
          ],
        ),
      ),
    );
  }
}

class SectionAboutTechnology extends StatelessWidget {
  const SectionAboutTechnology({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final double padding = MediaQuery.of(context).padding.left;
    return Padding(
      padding: EdgeInsets.symmetric(
        horizontal: kDefaultPadding + padding,
        vertical: kDefaultPadding / 2,
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          SectionTitle(
            boldText: "About",
            regularText: "Technology",
          ),
          SizedBox(height: kDefaultPadding / 2),
          Text(
            "I use flutter to build Luscious, flutter is very capable of building this kind of application. "
            "With the help of flutter bloc I can easily manage the state of the application. "
            "I also use local storage to store history, by using hydrated bloc is very easy, just add a few lines of code. "
            "Aside from saving recipe history also saves latest state from category list category, so when the category has been loaded the application does not need to reload the category.",
            textAlign: TextAlign.justify,
            style: kStyleNunitoRegular.copyWith(
              fontSize: 18,
              color: kBlack3Color,
            ),
          ),
        ],
      ),
    );
  }
}

class SectionAboutLuscious extends StatelessWidget {
  const SectionAboutLuscious({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final double padding = MediaQuery.of(context).padding.left;
    return Padding(
      padding: EdgeInsets.symmetric(
        horizontal: kDefaultPadding + padding,
        vertical: kDefaultPadding / 2,
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          SectionTitle(
            boldText: "About",
            regularText: "Luscious",
          ),
          SizedBox(height: kDefaultPadding / 2),
          Text(
            "Luscious is food recipe collection application powered by TheMealDB. "
            "You can explore recipes by category, or you can simply search for the meal name. "
            "We Save recipe you've viewed, so can easily back to that recipe.",
            textAlign: TextAlign.justify,
            style: kStyleNunitoRegular.copyWith(
              fontSize: 18,
              color: kBlack3Color,
            ),
          ),
        ],
      ),
    );
  }
}

class SectionAboutMe extends StatelessWidget {
  const SectionAboutMe({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final double padding = MediaQuery.of(context).padding.left;
    return Padding(
      padding: EdgeInsets.symmetric(
        horizontal: kDefaultPadding + padding,
        vertical: kDefaultPadding / 2,
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          MyProfile(),
          SizedBox(height: kDefaultPadding / 2),
          MyContact(),
        ],
      ),
    );
  }
}

class MyContact extends StatelessWidget {
  const MyContact({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        ContactItem(
          title: "Website",
          icon: "assets/images/website.png",
          account: "https://agungbudip.my.id",
          press: () async {
            await launch("https://agungbudip.my.id");
          },
        ),
        ContactItem(
          title: "Email",
          icon: "assets/images/gmail.png",
          account: "agungbudi3@gmail.com",
          press: () async {
            await launch(
              Uri(
                scheme: 'mailto',
                path: 'agungbudi3@gmail.com',
              ).toString(),
            );
          },
        ),
        ContactItem(
          title: "LikedIn",
          icon: "assets/images/linkedin.png",
          account: "Agung Budi",
          press: () async {
            await launch("https://www.linkedin.com/in/agung-budi-520451178/");
          },
        ),
        ContactItem(
          title: "Github",
          icon: "assets/images/github.png",
          account: "agungbudip",
          press: () async {
            await launch("https://github.com/agungbudip");
          },
        ),
        ContactItem(
          title: "Gitlab",
          icon: "assets/images/gitlab.png",
          account: "agungbudi3",
          press: () async {
            await launch("https://gitlab.com/agungbudi3");
          },
        ),
        ContactItem(
          title: "Twitter",
          icon: "assets/images/twitter.png",
          account: "agungbudip",
          press: () async {
            await launch("https://twitter.com/agungbudip");
          },
        ),
      ],
    );
  }
}

class MyProfile extends StatelessWidget {
  const MyProfile({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
          width: 100,
          height: 100,
          decoration: BoxDecoration(
            shape: BoxShape.circle,
            border: Border.all(
              color: kBlack1Color,
              width: 3,
            ),
          ),
          child: Container(
            decoration: BoxDecoration(
              shape: BoxShape.circle,
              image: DecorationImage(
                fit: BoxFit.fill,
                image: AssetImage("assets/images/avatar.png"),
              ),
            ),
          ),
        ),
        SizedBox(height: kDefaultPadding / 2),
        Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            FittedBox(
              fit: BoxFit.fitWidth,
              child: Text.rich(
                TextSpan(
                  text: "AGUNG",
                  style: kStyleNunitoBold.copyWith(
                    color: kBlack1Color,
                    fontSize: 24,
                  ),
                  children: [
                    TextSpan(
                      text: " BUDI",
                      style: kStyleNunitoRegular.copyWith(
                        color: kBlack3Color,
                      ),
                    ),
                    TextSpan(
                      text: " PRASETYO",
                    )
                  ],
                ),
                textAlign: TextAlign.center,
              ),
            ),
            FittedBox(
              fit: BoxFit.fitWidth,
              child: Text(
                "SOFTWARE ENGINEER | MOBILE APP DEVELOPER",
                style: kStyleNunitoRegular.copyWith(
                  color: kBlack3Color,
                  fontSize: 16,
                ),
              ),
            ),
          ],
        )
      ],
    );
  }
}

class ContactItem extends StatelessWidget {
  const ContactItem({
    Key key,
    @required this.title,
    @required this.icon,
    @required this.account,
    @required this.press,
  }) : super(key: key);

  final String title;
  final String icon;
  final String account;
  final VoidCallback press;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(bottom: 5),
      child: InkWell(
        onTap: press,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Container(
              child: Text(
                title,
                style: kStyleNunitoBold.copyWith(
                  fontSize: 16,
                  color: kBlack1Color,
                ),
              ),
            ),
            Container(
              width: 24,
              height: 24,
              padding: EdgeInsets.only(right: 5),
              child: Center(
                child: Image.asset(
                  icon,
                  width: 12,
                ),
              ),
            ),
            Text(
              account,
              style: kStyleNunitoRegular.copyWith(
                fontSize: 16,
                color: kAccentColor,
                decoration: TextDecoration.underline,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
