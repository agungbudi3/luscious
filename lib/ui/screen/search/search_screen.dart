import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:luscious/controller/search/search_bloc.dart';
import 'package:luscious/ui/screen/category/widgets/category_meal.dart';
import 'package:luscious/ui/screen/home/widgets/section_explore_categories.dart';
import 'package:luscious/untils/constants.dart';
import 'package:luscious/untils/routes.dart';

class SearchScreen extends StatelessWidget {
  const SearchScreen({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final Orientation orientation = MediaQuery.of(context).orientation;
    final bool isPortrait = orientation == Orientation.portrait;
    final double paddingHorizontal = MediaQuery.of(context).padding.left;
    var state = context.watch<SearchBloc>().state;
    if (state.isLoading) {
      return LoadingText("Searching Meals...");
    }
    if(state.searchQuery.isEmpty){
      return Container();
    }
    if (state.meals.length == 0) {
      return Container(
        padding: EdgeInsets.all(kDefaultPadding),
        child: Center(
          child: Text(
            "No Meal Found...!",
            style: kStyleNunitoSemiBold.copyWith(
              fontSize: 16,
              color: kAccentColor,
            ),
          ),
        ),
      );
    }
    return Container(
      padding: EdgeInsets.symmetric(
        horizontal: isPortrait
            ? paddingHorizontal
            : paddingHorizontal + kDefaultPadding,
        vertical: kDefaultPadding,
      ),
      child: GridView.builder(
        itemCount: state.meals.length,
        physics: NeverScrollableScrollPhysics(),
        shrinkWrap: true,
        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
            crossAxisCount: isPortrait ? 1 : 2,
            mainAxisSpacing: kDefaultPadding,
            crossAxisSpacing: kDefaultPadding,
            childAspectRatio: 16 / 9),
        itemBuilder: (ctx, idx) {
          var meal = state.meals[idx];
          return InkWell(
            splashColor: kAccentColor,
            onTap: () {
              Navigator.pushNamed(
                ctx,
                detailRoute,
                arguments: meal,
              );
            },
            child: CategoryMeal(
              image: meal.strMealThumb,
              name: meal.strMeal,
              id: meal.idMeal,
            ),
          );
        },
      ),
    );
  }
}
