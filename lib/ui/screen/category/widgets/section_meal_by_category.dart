import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:luscious/controller/meal_category/meal_category_bloc.dart';
import 'package:luscious/ui/screen/category/widgets/category_meal.dart';
import 'package:luscious/ui/screen/home/widgets/section_explore_categories.dart';
import 'package:luscious/untils/constants.dart';
import 'package:luscious/untils/network/endpoints.dart';
import 'package:luscious/untils/routes.dart';

class SectionMealByCategory extends StatelessWidget {
  const SectionMealByCategory({
    Key key,
    @required this.category,
  }) : super(key: key);
  final String category;

  @override
  Widget build(BuildContext context) {
    final double paddingHorizontal = MediaQuery.of(context).padding.left;
    final Orientation orientation = MediaQuery.of(context).orientation;
    final bool isPortrait = orientation == Orientation.portrait;
    return BlocProvider(
      create: (c) => MealCategoryBloc(
        RepositoryProvider.of<Endpoints>(c),
      )..add(OnLoadMealByCategory(category)),
      child: Container(
        padding: EdgeInsets.symmetric(
          vertical: kDefaultPadding,
          horizontal: paddingHorizontal,
        ),
        child: Builder(
          builder: (c) {
            var state = c.watch<MealCategoryBloc>().state;
            if (state.isLoading) {
              return LoadingText("Loading Meals...");
            }
            if (state.meals.length == 0) {
              return Container();
            }
            return GridView.builder(
              itemCount: state.meals.length,
              physics: NeverScrollableScrollPhysics(),
              shrinkWrap: true,
              gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                  crossAxisCount: isPortrait ? 1 : 2,
                  mainAxisSpacing: kDefaultPadding,
                  crossAxisSpacing: kDefaultPadding,
                  childAspectRatio: 16 / 9),
              itemBuilder: (ctx, idx) {
                var meal = state.meals[idx];
                return InkWell(
                  splashColor: kAccentColor,
                  onTap: () {
                    Navigator.pushNamed(
                      ctx,
                      detailRoute,
                      arguments: meal,
                    );
                  },
                  child: CategoryMeal(
                    image: meal.strMealThumb,
                    name: meal.strMeal,
                    id: meal.idMeal,
                  ),
                );
              },
            );
          },
        ),
      ),
    );
  }
}
