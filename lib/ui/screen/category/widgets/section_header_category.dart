import 'package:flutter/material.dart';
import 'package:luscious/ui/widgets/category_image.dart';
import 'package:luscious/untils/constants.dart';

class SectionHeaderCategory extends StatelessWidget {
  const SectionHeaderCategory({
    Key key,
    @required this.id,
    @required this.image,
    @required this.name,
  }) : super(key: key);

  final String id, image, name;

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(kDefaultPadding),
      decoration: BoxDecoration(
        color: kAccentColor,
        image: DecorationImage(
            image: AssetImage("assets/images/food_pattern.png"),
            repeat: ImageRepeat.repeat,
            scale: 3.0),
      ),
      child: Column(
        children: [
          Padding(
            padding: const EdgeInsets.all(kDefaultPadding),
            child: Center(
              child: ClipRRect(
                borderRadius: BorderRadius.circular(kDefaultPadding),
                child: CategoryImage(
                  id: id,
                  image: image,
                  width: 300,
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
