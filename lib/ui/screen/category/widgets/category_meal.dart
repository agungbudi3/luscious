import 'package:flutter/material.dart';
import 'package:luscious/ui/screen/home/widgets/blurred_background_title.dart';
import 'package:luscious/ui/widgets/meal_image.dart';
import 'package:luscious/untils/constants.dart';

class CategoryMeal extends StatelessWidget {
  const CategoryMeal({
    Key key,
    @required this.image,
    @required this.name,
    @required this.id,
  }) : super(key: key);

  final String image, name, id;

  @override
  Widget build(BuildContext context) {
    final Orientation orientation = MediaQuery.of(context).orientation;
    final bool isPortrait = orientation == Orientation.portrait;
    return Container(
      margin: !isPortrait
          ? null
          : EdgeInsets.symmetric(horizontal: kDefaultPadding),
      decoration: BoxDecoration(
        color: kAccentColor,
        borderRadius: BorderRadius.circular(
          kDefaultPadding / 2,
        ),
        boxShadow: [
          BoxShadow(
            color: Color(0x26000000),
            spreadRadius: 0,
            blurRadius: 6,
            offset: Offset(3, 3),
          ),
        ],
      ),
      child: Stack(
        children: [
          Positioned(
            bottom: 0,
            left: 0,
            right: 0,
            top: 0,
            child: ClipRRect(
              borderRadius: BorderRadius.circular(
                kDefaultPadding / 2,
              ),
              child: MealImage(id: id, image: image, useLargeImage: true,),
            ),
          ),
          Positioned(
            bottom: 0,
            left: 0,
            right: 0,
            child: BlurredBackgroundTitle(
              name,
              style: kStyleNunitoBold.copyWith(
                color: Colors.white,
                fontSize: 20,
              ),
            ),
          ),
        ],
      ),
    );
  }
}