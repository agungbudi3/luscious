
import 'package:flutter/material.dart';
import 'package:luscious/models/category.dart';
import 'package:luscious/ui/screen/category/widgets/section_header_category.dart';
import 'package:luscious/ui/screen/category/widgets/section_meal_by_category.dart';
import 'package:luscious/ui/widgets/my_custom_appbar.dart';

class CategoryScreen extends StatelessWidget {
  const CategoryScreen({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final Category category = ModalRoute.of(context).settings.arguments;
    final double paddingVertical = MediaQuery.of(context).padding.top;
    return Scaffold(
      appBar: MyCustomAppBar(
        padding: paddingVertical,
        title: category.strCategory,
      ),
      body: SingleChildScrollView(
        physics: ScrollPhysics(),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            SectionHeaderCategory(
              id: category.idCategory,
              image: category.strCategoryThumb,
              name: category.strCategory,
            ),
            SectionMealByCategory(
              category: category.strCategory,
            )
          ],
        ),
      ),
    );
  }
}
