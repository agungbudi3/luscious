import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:luscious/controller/detail/detail_bloc.dart';
import 'package:luscious/controller/recent/recent_cubit.dart';
import 'package:luscious/models/meal.dart';
import 'package:luscious/ui/screen/detail/widgets/section_instruction.dart';
import 'package:luscious/ui/screen/detail/widgets/section_meal_image.dart';
import 'package:luscious/ui/screen/detail/widgets/section_meal_ingredient.dart';
import 'package:luscious/ui/screen/detail/widgets/section_meal_name.dart';
import 'package:luscious/ui/screen/home/widgets/section_explore_categories.dart';
import 'package:luscious/ui/widgets/my_custom_appbar.dart';
import 'package:luscious/untils/network/endpoints.dart';

class DetailScreen extends StatelessWidget {
  DetailScreen({Key key}) : super(key: key);
  final ValueNotifier<bool> transparentNotifier = ValueNotifier(true);
  final ScrollController _scrollController = ScrollController();

  @override
  Widget build(BuildContext context) {
    final Meal meal = ModalRoute.of(context).settings.arguments;
    final paddingTop = MediaQuery.of(context).padding.top;
    final screenWidth = MediaQuery.of(context).size.width;
    return BlocProvider(
      create: (c) => DetailBloc(
        RepositoryProvider.of<Endpoints>(c),
      )..add(OnLoadMeal(meal.idMeal)),
      child: Scaffold(
        body: NotificationListener(
          onNotification: (t) {
            if (t is ScrollUpdateNotification) {
              var current =
                  _scrollController.position.pixels; /*t.metrics.pixels;*/
              if (current >= screenWidth - (56 * 2) - paddingTop &&
                  transparentNotifier.value) {
                transparentNotifier.value = false;
              } else if (current < screenWidth - (56 * 2) - paddingTop &&
                  !transparentNotifier.value) {
                transparentNotifier.value = true;
              }
            }
            return true;
          },
          child: BlocListener<DetailBloc, DetailState>(
            listener: (c, s){
              if(s is DetailSuccess){
                c.read<RecentCubit>().addMeal(s.meal);
              }
            },
            child: Stack(
              children: [
                Positioned(
                  top: 0,
                  bottom: 0,
                  left: 0,
                  right: 0,
                  child: SingleChildScrollView(
                    controller: _scrollController,
                    physics: ScrollPhysics(),
                    child: Builder(builder: (c) {
                      var state = c.watch<DetailBloc>().state;
                      return Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          SectionMealImage(
                            id: meal.idMeal,
                            image: meal.strMealThumb,
                          ),
                          if (state is DetailLoading) ...[
                            LoadingText("Loading Meal..."),
                          ],
                          if (state is DetailSuccess) ...[
                            SectionMealName(
                              name: meal.strMeal,
                              category: state.meal.strCategory,
                              area: state.meal.strArea,
                            ),
                            SectionMealIngredient(
                              ingredients: state.meal.ingredients,
                            ),
                            SectionInstruction(
                              instructions: state.meal.instructions,
                            ),
                          ],
                        ],
                      );
                    }),
                  ),
                ),
                Positioned(
                  top: 0,
                  left: 0,
                  right: 0,
                  child: ValueListenableBuilder<bool>(
                    valueListenable: transparentNotifier,
                    builder: (ctx, isTransparent, c) {
                      return MyCustomAppBar(
                        padding: paddingTop,
                        title: meal.strMeal,
                        isTransParent: isTransparent,
                      );
                    },
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
