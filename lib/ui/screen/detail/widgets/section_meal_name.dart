import 'package:flutter/material.dart';
import 'package:luscious/untils/constants.dart';

class SectionMealName extends StatelessWidget {
  const SectionMealName({
    Key key,
    this.name,
    this.category,
    this.area,
  }) : super(key: key);

  final String name, category, area;

  @override
  Widget build(BuildContext context) {
    final paddingHorizontal = MediaQuery.of(context).padding.left;
    return Container(
      width: double.infinity,
      padding: EdgeInsets.only(
        left: kDefaultPadding + paddingHorizontal,
        right: kDefaultPadding + paddingHorizontal,
        bottom: kDefaultPadding/2,
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            name,
            style: kStyleNunitoExtraBold.copyWith(
              fontSize: 24,
              color: kBlack1Color,
            ),
          ),
          Text(
            "$category | $area",
            style: kStyleNunitoRegular.copyWith(
              fontSize: 14,
              color: kBlack3Color,
            ),
          ),
        ],
      ),
    );
  }
}