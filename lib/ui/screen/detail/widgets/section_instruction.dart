import 'package:flutter/material.dart';
import 'package:luscious/untils/constants.dart';

class SectionInstruction extends StatelessWidget {
  const SectionInstruction({
    Key key,
    @required this.instructions,
  }) : super(key: key);

  final List<String> instructions;

  @override
  Widget build(BuildContext context) {

    final paddingHorizontal = MediaQuery.of(context).padding.left;
    return Container(
      padding: EdgeInsets.symmetric(
        horizontal: kDefaultPadding + paddingHorizontal,
        vertical: kDefaultPadding/2,
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            "Instruction",
            style: kStyleNunitoExtraBold.copyWith(
              fontSize: 18,
              color: kBlack1Color,
            ),
          ),
          ListView.builder(
            physics: NeverScrollableScrollPhysics(),
            shrinkWrap: true,
            itemCount: instructions.length,
            padding: EdgeInsets.zero,
            itemBuilder: (ctx, idx) {
              return Container(
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(kDefaultPadding / 2),
                  color: kAccentColor.withOpacity(0.2),
                ),
                padding: EdgeInsets.all(kDefaultPadding / 2),
                margin: EdgeInsets.symmetric(vertical: kDefaultPadding / 2),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      "Step ${idx + 1}",
                      style: kStyleNunitoExtraBold.copyWith(
                        color: kAccentColor,
                        fontSize: 16,
                      ),
                    ),
                    SizedBox(
                      height: kDefaultPadding / 2,
                    ),
                    Text(
                      "${instructions[idx]}.",
                      style: kStyleNunitoRegular.copyWith(
                        color: kBlack1Color,
                        fontSize: 16,
                      ),
                    )
                  ],
                ),
              );
            },
          )
        ],
      ),
    );
  }
}