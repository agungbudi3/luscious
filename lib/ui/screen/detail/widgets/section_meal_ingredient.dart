import 'package:flutter/material.dart';
import 'package:luscious/models/ingredient.dart';
import 'package:luscious/ui/screen/detail/widgets/ingredient_info.dart';
import 'package:luscious/untils/constants.dart';

class SectionMealIngredient extends StatelessWidget {
  const SectionMealIngredient({
    Key key,
    @required this.ingredients,
  }) : super(key: key);

  final List<Ingredient> ingredients;

  @override
  Widget build(BuildContext context) {
    final paddingHorizontal = MediaQuery.of(context).padding.left;
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Container(
          padding: EdgeInsets.only(
            top: kDefaultPadding,
            left: kDefaultPadding + paddingHorizontal,
            right: kDefaultPadding + paddingHorizontal,
          ),
          child: Row(
            mainAxisSize: MainAxisSize.max,
            children: [
              Expanded(
                child: Text(
                  "Ingredients",
                  style: kStyleNunitoExtraBold.copyWith(
                    fontSize: 18,
                    color: kBlack1Color,
                  ),
                ),
              ),
              Text(
                "${ingredients.length} item(s)",
                style: kStyleNunitoExtraBold.copyWith(
                  fontSize: 16,
                  color: kBlack3Color,
                ),
              ),
            ],
          ),
        ),
        SizedBox(height: kDefaultPadding / 2),
        Container(
          height: 190,
          child: ListView.builder(
            padding: EdgeInsets.symmetric(
              horizontal: kDefaultPadding - kDefaultPadding / 2 + paddingHorizontal,
            ),
            scrollDirection: Axis.horizontal,
            shrinkWrap: true,
            itemCount: ingredients.length,
            itemBuilder: (ctx, idx) {
              var ingredient = ingredients[idx];
              return IngredientInfo(
                ingredient: ingredient.ingredient,
                measure: ingredient.measurement,
              );
            },
          ),
        ),
      ],
    );
  }
}