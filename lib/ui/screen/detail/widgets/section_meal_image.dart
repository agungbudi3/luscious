import 'package:flutter/material.dart';
import 'package:luscious/ui/widgets/meal_image.dart';
import 'package:luscious/untils/constants.dart';

class SectionMealImage extends StatelessWidget {
  const SectionMealImage({
    Key key,
    @required this.id,
    @required this.image,
  }) : super(key: key);

  final String id, image;

  @override
  Widget build(BuildContext context) {
    final screenWidth = MediaQuery.of(context).size.width;
    return Stack(
      children: [
        AspectRatio(
          aspectRatio: 1,
          child: MealImage(
            id: id,
            image: image,
            useLargeImage: true,
          ),
        ),
        Positioned(
          top: screenWidth - kDefaultPadding,
          left: 0,
          right: 0,
          child: Container(
            height: 40,
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.only(
                topRight: Radius.circular(kDefaultPadding),
                topLeft: Radius.circular(kDefaultPadding),
              ),
            ),
          ),
        ),
      ],
    );
  }
}