import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:luscious/untils/constants.dart';

class IngredientInfo extends StatelessWidget {
  const IngredientInfo({
    Key key,
    this.ingredient,
    this.measure,
  }) : super(key: key);

  final String ingredient, measure;

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 120,
      padding: EdgeInsets.symmetric(horizontal: kDefaultPadding / 2),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            width: 100,
            height: 100,
            decoration: BoxDecoration(
              color: kGray2Color,
              borderRadius: BorderRadius.circular(kDefaultPadding),
            ),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Container(
                  width: 50,
                  height: 50,
                  child: CachedNetworkImage(
                    imageUrl: "https://www.themealdb.com/images/ingredients/$ingredient-Small.png",
                    placeholder: (_,__){
                      return Padding(
                        padding: EdgeInsets.all(15.0),
                        child: CircularProgressIndicator(),
                      );
                    },
                    errorWidget: (_,__,___){
                      return Icon(Icons.landscape, color: kAccentColor, size: 50,);
                    },
                  ),
                ),
              ],
            ),
          ),
          SizedBox(height: kDefaultPadding / 2),
          Padding(
            padding: EdgeInsets.symmetric(horizontal: kDefaultPadding / 2),
            child: Text(
              ingredient,
              maxLines: 2,
              overflow: TextOverflow.ellipsis,
              style: kStyleNunitoExtraBold.copyWith(
                color: kBlack1Color,
                fontSize: 14,
              ),
            ),
          ),
          Padding(
            padding: EdgeInsets.symmetric(horizontal: kDefaultPadding / 2),
            child: Text(
              measure,
              maxLines: 2,
              overflow: TextOverflow.ellipsis,
              style: kStyleNunitoRegular.copyWith(
                color: kBlack1Color,
                fontSize: 14,
              ),
            ),
          ),
        ],
      ),
    );
  }
}