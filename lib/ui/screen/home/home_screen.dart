
import 'package:flutter/material.dart';
import 'package:luscious/controller/search/search_bloc.dart';
import 'package:luscious/ui/screen/search/search_screen.dart';
import 'package:luscious/ui/widgets/search_app_bar.dart';
import 'package:luscious/ui/screen/home/widgets/section_explore_categories.dart';
import 'package:luscious/ui/screen/home/widgets/section_recipe_you_have_viewed.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class HomeScreen extends StatelessWidget {
  const HomeScreen({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: SearchAppBar(
        paddingTop: MediaQuery.of(context).padding.top,
      ),
      body: SingleChildScrollView(
        physics: ScrollPhysics(),
        child: Builder(
          builder: (c){
            var state = c.watch<SearchBloc>().state;
            if(state.isSearch){
              return SearchScreen();
            }
            return Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SectionRecipeYouHaveViewed(),
                SectionExploreCategories(),
              ],
            );
          },
        ),
      ),
    );
  }
}
