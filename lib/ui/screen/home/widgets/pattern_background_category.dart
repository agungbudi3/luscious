import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:luscious/ui/widgets/category_image.dart';
import 'package:luscious/untils/constants.dart';

class PatternBackgroundCategory extends StatelessWidget {
  const PatternBackgroundCategory({
    Key key,
    @required this.image,
    @required this.category,
    @required this.id,
  }) : super(key: key);

  final String id;
  final String image;
  final String category;

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.all(kDefaultPadding / 2),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(kDefaultPadding / 2),
        color: kAccentColor,
        image: DecorationImage(
          image: AssetImage("assets/images/food_pattern.png"),
          repeat: ImageRepeat.repeat,
        ),
        boxShadow: [
          BoxShadow(
            color: Color(0x26000000),
            spreadRadius: 1,
            blurRadius: 6,
            offset: Offset(3, 4),
          ),
        ],
      ),
      child: Padding(
        padding: EdgeInsets.all(kDefaultPadding / 2),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            ClipRRect(
              borderRadius: BorderRadius.circular(kDefaultPadding),
              child: CategoryImage(
                id: id,
                image: image,
              ),
            ),
            SizedBox(height: kDefaultPadding / 2),
            Text(
              category,
              textAlign: TextAlign.center,
              maxLines: 1,
              overflow: TextOverflow.ellipsis,
              style: kStyleNunitoExtraBold.copyWith(
                color: Colors.white,
                fontSize: 18,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
