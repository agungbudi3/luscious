import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:luscious/ui/screen/home/widgets/blurred_background_title.dart';
import 'package:luscious/untils/constants.dart';

class RecentMeal extends StatelessWidget {
  const RecentMeal({
    Key key,
    this.id,
    this.image,
    this.name = '-',
    this.height = 100,
  }) : super(key: key);
  final String id, image,name;
  final double height;

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 16 / 9 * height,
      height: height,
      margin: EdgeInsets.symmetric(
        horizontal: kDefaultPadding / 2,
        vertical: kDefaultPadding / 2,
      ),
      decoration: BoxDecoration(
        color: kAccentColor.withOpacity(0.5),
        borderRadius: BorderRadius.circular(
          kDefaultPadding / 2,
        ),
        image: image == null
            ? null
            : DecorationImage(
                image: CachedNetworkImageProvider(
                  image,
                ),
                fit: BoxFit.cover,
              ),
        boxShadow: [
          BoxShadow(
            color: Color(0x26000000),
            spreadRadius: 0,
            blurRadius: 6,
            offset: Offset(3, 3),
          ),
        ],
      ),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.end,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          BlurredBackgroundTitle(name),
        ],
      ),
    );
  }
}
