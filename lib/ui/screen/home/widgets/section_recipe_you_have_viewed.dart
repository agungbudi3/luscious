import 'package:flutter/material.dart';
import 'package:luscious/controller/recent/recent_cubit.dart';
import 'package:luscious/ui/screen/home/widgets/recent_meal.dart';
import 'package:luscious/ui/widgets/section_title.dart';
import 'package:luscious/untils/constants.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:luscious/untils/routes.dart';

class SectionRecipeYouHaveViewed extends StatelessWidget {
  const SectionRecipeYouHaveViewed({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    double paddingHorizontal = MediaQuery.of(context).padding.left;
    var state = context.watch<RecentCubit>().state;
    if(state.recentMeal.isEmpty){
      return Container();
    }
    return Container(
      padding: EdgeInsets.only(
        top: kDefaultPadding,
        bottom: kDefaultPadding / 2,
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Padding(
            padding: EdgeInsets.symmetric(
              horizontal: kDefaultPadding + paddingHorizontal,
            ),
            child: SectionTitle(
              boldText: "Recipes",
              regularText: "You've Viewed",
            ),
          ),
          SizedBox(height: kDefaultPadding / 2),
          Container(
            height: 170,
            child: ListView.builder(
              scrollDirection: Axis.horizontal,
              shrinkWrap: true,
              itemCount: state.recentMeal.length,
              padding: EdgeInsets.symmetric(
                horizontal: kDefaultPadding / 2 + paddingHorizontal,
              ),
              itemBuilder: (ctx, idx) {
                var meal = state.recentMeal[idx];
                return InkWell(
                  onTap: (){
                    Navigator.pushNamed(ctx, detailRoute, arguments: meal);
                  },
                  child: RecentMeal(
                    height: 150,
                    id: meal.idMeal,
                    image: meal.strMealThumb,
                    name: meal.strMeal,
                  ),
                );
              },
            ),
          )
        ],
      ),
    );
  }
}
