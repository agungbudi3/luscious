import 'package:flutter/material.dart';
import 'package:luscious/controller/category/category_bloc.dart';
import 'package:luscious/ui/screen/home/widgets/pattern_background_category.dart';
import 'package:luscious/ui/widgets/section_title.dart';
import 'package:luscious/untils/constants.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:luscious/untils/routes.dart';

class SectionExploreCategories extends StatefulWidget {
  const SectionExploreCategories({
    Key key,
  }) : super(key: key);

  @override
  _SectionExploreCategoriesState createState() =>
      _SectionExploreCategoriesState();
}

class _SectionExploreCategoriesState extends State<SectionExploreCategories> {
  @override
  void initState() {
    super.initState();
    if (context.read<CategoryBloc>().state.categories.length == 0) {
      context.read<CategoryBloc>().add(OnLoadCategory());
    }
  }

  @override
  Widget build(BuildContext context) {
    Orientation orientation = MediaQuery.of(context).orientation;
    double paddingHorizontal = MediaQuery.of(context).padding.left;
    bool isPortrait = orientation == Orientation.portrait;
    return Container(
      padding: EdgeInsets.symmetric(vertical: kDefaultPadding),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Padding(
            padding: EdgeInsets.symmetric(
              horizontal: kDefaultPadding + paddingHorizontal,
            ),
            child: SectionTitle(
              boldText: "Explore",
              regularText: "Categories",
            ),
          ),
          SizedBox(height: kDefaultPadding / 2),
          Container(
            padding: EdgeInsets.symmetric(
              horizontal: kDefaultPadding / 2 + paddingHorizontal,
            ),
            child: Builder(
              builder: (c) {
                var state = c.watch<CategoryBloc>().state;
                if (state.isLoading) {
                  return LoadingText("Loading Categories...");
                }
                return GridView.builder(
                  itemCount: state.categories.length,
                  physics: NeverScrollableScrollPhysics(),
                  shrinkWrap: true,
                  gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                      crossAxisCount: isPortrait ? 2 : 4,
                      crossAxisSpacing: kDefaultPadding / 2,
                      mainAxisSpacing: kDefaultPadding / 2,
                      childAspectRatio: 1 / 1.2),
                  itemBuilder: (ctx, idx) {
                    var category = state.categories[idx];
                    return InkWell(
                      splashColor: kAccentColor,
                      onTap: () {
                        Navigator.pushNamed(
                          ctx,
                          categoryRoute,
                          arguments: category,
                        );
                      },
                      child: PatternBackgroundCategory(
                        image: category.strCategoryThumb,
                        category: category.strCategory,
                        id: category.idCategory,
                      ),
                    );
                  },
                );
              },
            ),
          )
        ],
      ),
    );
  }
}

class LoadingText extends StatelessWidget {
  const LoadingText(
    this.text, {
    Key key,
  }) : super(key: key);
  final String text;

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(kDefaultPadding),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Container(
            width: 20,
            height: 20,
            child: CircularProgressIndicator(
              valueColor: AlwaysStoppedAnimation<Color>(kAccentColor),
            ),
          ),
          SizedBox(width: kDefaultPadding / 2),
          Text(
            text,
            style: kStyleNunitoSemiBold.copyWith(
              fontSize: 16,
              color: kAccentColor,
            ),
          ),
        ],
      ),
    );
  }
}
