import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:luscious/untils/constants.dart';

class BlurredBackgroundTitle extends StatelessWidget {
  const BlurredBackgroundTitle(
    this.title, {
    Key key,
    this.textAlign = TextAlign.start,
    this.padding,
    this.style,
  }) : super(key: key);
  final String title;
  final TextAlign textAlign;
  final EdgeInsetsGeometry padding;
  final TextStyle style;

  @override
  Widget build(BuildContext context) {
    return ClipRRect(
      borderRadius: BorderRadius.only(
        bottomLeft: Radius.circular(kDefaultPadding / 2),
        bottomRight: Radius.circular(kDefaultPadding / 2),
      ),
      child: Container(
        color: kAccentColor.withOpacity(0.3),
        child: BackdropFilter(
          filter: ImageFilter.blur(
            sigmaX: 4,
            sigmaY: 4,
          ),
          child: Container(
            width: double.infinity,
            padding: padding != null
                ? padding
                : EdgeInsets.only(
              top: kDefaultPadding / 4,
              bottom: kDefaultPadding / 2,
              left: kDefaultPadding / 2,
              right: kDefaultPadding / 2,
            ),
            child: Text(
              title,
              textAlign: textAlign,
              maxLines: 2,
              overflow: TextOverflow.ellipsis,
              style: style != null
                  ? style
                  : kStyleNunitoExtraBold.copyWith(
                color: Colors.white,
                fontSize: 16,
              ),
            ),
          ),
        ),
      ),
    );
  }
}
