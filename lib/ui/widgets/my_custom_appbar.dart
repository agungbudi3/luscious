import 'dart:ui';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:luscious/untils/constants.dart';

class MyCustomAppBar extends StatelessWidget implements PreferredSizeWidget {
  const MyCustomAppBar({
    Key key,
    @required this.padding,
    this.height = 56,
    this.title = "",
    this.isTransParent = false,
  }) : super(key: key);

  final double padding, height;
  final String title;
  final bool isTransParent;

  @override
  Widget build(BuildContext context) {
    final double paddingHorizontal = MediaQuery.of(context).padding.left;
    return AnimatedContainer(
      duration: Duration(milliseconds: 200),
      height: height + padding,
      padding: EdgeInsets.only(
        top: padding,
        left: paddingHorizontal,
        right: paddingHorizontal,
      ),
      decoration: BoxDecoration(
        color: isTransParent ? Colors.transparent : Colors.white,
        boxShadow: [
          BoxShadow(
            color: Color(0x26000000),
            spreadRadius: 1,
            blurRadius: 6,
            offset: Offset(-3, -3),
          ),
        ],
      ),
      child: Stack(
        children: [
          Positioned(
            top: 0,
            bottom: 0,
            left: 0,
            child: Material(
              color: Colors.transparent,
              child: InkWell(
                splashColor: kAccentColor,
                onTap: () {
                  Navigator.pop(context);
                },
                child: Container(
                  height: height,
                  padding: EdgeInsets.symmetric(
                    horizontal: kDefaultPadding / 2,
                  ),
                  child: Row(
                    children: [
                      Icon(
                        Icons.chevron_left,
                        color: kAccentColor,
                        size: 24,
                      ),
                      Padding(
                        padding: EdgeInsets.only(
                          right: kDefaultPadding / 2,
                        ),
                        child: Text(
                          "Back",
                          style: kStyleNunitoBold.copyWith(
                            fontSize: 16,
                            color: kAccentColor,
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ),
          Positioned(
            top: 0,
            bottom: 0,
            left: 90,
            right: 90,
            child: Container(
              height: height,
              child: Center(
                child: Text(
                  isTransParent?"":title,
                  textAlign: TextAlign.center,
                  maxLines: 2,
                  overflow: TextOverflow.ellipsis,
                  style: kStyleNunitoBlack.copyWith(
                    color: kBlack1Color,
                    fontSize: 16,
                  ),
                ),
              ),
            ),
          )
        ],
      ),
    );
  }

  @override
  Size get preferredSize => Size.fromHeight(height + padding);
}
