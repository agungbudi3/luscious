import 'package:flutter/material.dart';
import 'package:luscious/controller/search/search_bloc.dart';
import 'package:luscious/untils/constants.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class SearchField extends StatefulWidget {
  const SearchField({Key key}) : super(key: key);

  @override
  _SearchFieldState createState() => _SearchFieldState();
}

class _SearchFieldState extends State<SearchField> {
  FocusNode _focus = new FocusNode();
  TextEditingController _controller = new TextEditingController();

  @override
  void initState() {
    super.initState();
    _focus.addListener(_onFocusChange);
  }

  @override
  void dispose() {
    super.dispose();
    _focus.dispose();
  }

  void _onFocusChange() {
    //debugPrint("Focus: " + _focus.hasFocus.toString());
    if(_focus.hasFocus){
      context.read<SearchBloc>().add(OnSearchFocusChange(true));
    }
  }

  @override
  Widget build(BuildContext context) {
    return BlocListener<SearchBloc, SearchState>(
      listener: (c, s){
        if(!s.isSearch){
          FocusScope.of(context).unfocus();
          _controller.clear();
        }
      },
      child: TextField(
        controller: _controller,
        focusNode: _focus,
        autofocus: false,
        cursorColor: kAccentColor,
        style: kStyleNunitoRegular.copyWith(
          fontSize: 16,
          color: kBlack1Color,
        ),
        textInputAction: TextInputAction.search,
        onSubmitted: (value) {
          context.read<SearchBloc>().add(OnSearchMeal(value));
        },
        decoration: InputDecoration(
          contentPadding: EdgeInsets.symmetric(
            horizontal: kDefaultPadding,
            vertical: kDefaultPadding / 2,
          ),
          hintText: "Search your meal...",
          hintStyle: kStyleNunitoRegular.copyWith(
            fontSize: 16,
            color: kBlack3Color,
          ),
          filled: true,
          fillColor: kGray1Color,
          border: _inputBorder(),
          focusedBorder: _inputBorder(),
          enabledBorder: _inputBorder(),
          errorBorder: _inputBorder(),
          disabledBorder: _inputBorder(),
        ),
      ),
    );
  }

  OutlineInputBorder _inputBorder() {
    return OutlineInputBorder(
      borderRadius: BorderRadius.circular(8),
      borderSide: BorderSide(
        width: 0,
        style: BorderStyle.none,
      ),
    );
  }
}
