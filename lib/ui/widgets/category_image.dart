import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:luscious/untils/constants.dart';

class CategoryImage extends StatelessWidget {
  const CategoryImage({
    Key key,
    @required this.image,
    @required this.id,
    this.width = 150,
  }) : super(key: key);

  final String image;
  final String id;
  final double width;

  @override
  Widget build(BuildContext context) {
    return Container(
      width: width,
      child: Hero(
        tag: "category-$id",
        child: CachedNetworkImage(
          imageUrl: image,
          placeholder: (_, __){
            return Container(
              padding: EdgeInsets.all(kDefaultPadding),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  CircularProgressIndicator(
                    valueColor: AlwaysStoppedAnimation<Color>(Colors.white),
                  )
                ],
              ),
            );
          },
          errorWidget: (_, __, ___){
            return Icon(
              Icons.landscape,
              size: width,
              color: kPrimaryColor,
            );
          },
        ),
      ),
    );
  }
}