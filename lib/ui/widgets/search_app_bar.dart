import 'package:flutter/material.dart';
import 'package:luscious/controller/search/search_bloc.dart';
import 'package:luscious/ui/widgets/search_field.dart';
import 'package:luscious/untils/constants.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:luscious/untils/routes.dart';

class SearchAppBar extends StatelessWidget implements PreferredSizeWidget {
  const SearchAppBar({
    Key key,
    this.paddingTop = 0,
  }) : super(key: key);

  final double paddingTop;
  final double barHeight = 70;

  @override
  Widget build(BuildContext context) {
    double paddingHorizontal = MediaQuery.of(context).padding.left;
    return Container(
      height: barHeight + paddingTop,
      padding: EdgeInsets.only(
          top: kDefaultPadding / 2 + paddingTop,
          left: kDefaultPadding + paddingHorizontal,
          right: kDefaultPadding + paddingHorizontal,
          bottom: kDefaultPadding / 2),
      decoration: BoxDecoration(
        color: kPrimaryColor,
        boxShadow: [
          BoxShadow(
            color: Color(0x26000000),
            spreadRadius: 1,
            blurRadius: 6,
            offset: Offset(-3, -3),
          ),
        ],
      ),
      child: Row(
        children: [
          Expanded(
            child: SearchField(),
          ),
          SizedBox(width: kDefaultPadding / 2),
          Builder(builder: (c) {
            var state = c.watch<SearchBloc>().state;
            if (state.isSearch) {
              return TextButton(
                  onPressed: () {
                    c.read<SearchBloc>().add(OnSearchFocusChange(false));
                  },
                  child: Text(
                    "Cancel",
                    style: kStyleNunitoBold.copyWith(
                      fontSize: 16,
                      color: kAccentColor,
                    ),
                  ));
            }
            return Container(
              width: barHeight - kDefaultPadding,
              height: barHeight - kDefaultPadding,
              child: Material(
                color: kGray1Color,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(
                    kDefaultPadding / 2,
                  ),
                ),
                clipBehavior: Clip.hardEdge,
                child: InkWell(
                  splashColor: kAccentColor,
                  onTap: () {
                    Navigator.pushNamed(context, aboutRoute);
                  },
                  child: Icon(
                    Icons.account_circle_outlined,
                    color: kAccentColor,
                  ),
                ),
              ),
            );
          }),
        ],
      ),
    );
  }

  @override
  Size get preferredSize => Size.fromHeight(barHeight + paddingTop);
}
