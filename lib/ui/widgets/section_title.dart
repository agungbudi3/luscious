import 'package:flutter/material.dart';
import 'package:luscious/untils/constants.dart';

class SectionTitle extends StatelessWidget {
  const SectionTitle({
    Key key,
    @required this.boldText,
    @required this.regularText,
  }) : super(key: key);

  final String boldText;
  final String regularText;

  @override
  Widget build(BuildContext context) {
    return RichText(
      text: TextSpan(
        text: "$boldText ",
        style: kStyleNunitoExtraBold.copyWith(
          fontSize: 18,
          color: kBlack1Color,
        ),
        children: [
          TextSpan(
            text: regularText,
            style: kStyleNunitoRegular,
          ),
        ],
      ),
    );
  }
}