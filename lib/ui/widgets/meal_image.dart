import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:luscious/untils/constants.dart';

class MealImage extends StatelessWidget {
  const MealImage({
    Key key,
    @required this.id,
    @required this.image,
    this.useLargeImage = false,
  }) : super(key: key);

  final String id;
  final String image;
  final bool useLargeImage;

  @override
  Widget build(BuildContext context) {
    return Hero(
      tag: "meal-$id",
      child: CachedNetworkImage(
        fit: BoxFit.cover,
        imageUrl: useLargeImage?image:"$image/preview",
        placeholder: (_, __){
          return Container(
            decoration: BoxDecoration(
              color: kAccentColor,
            ),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                CircularProgressIndicator(
                  valueColor: AlwaysStoppedAnimation<Color>(Colors.white),
                )
              ],
            ),
          );
        },
        errorWidget: (_, __, ___){
          return Container(
            color: kAccentColor,
            child: Icon(
              Icons.landscape,
              size: 100,
              color: kPrimaryColor,
            ),
          );
        },
      ),
      /*child: Image.network(
        useLargeImage?image:"$image/preview",
        fit: BoxFit.cover,
        loadingBuilder: (_, child, loadingProgress) {
          if (loadingProgress == null) return child;
          return Container(
            decoration: BoxDecoration(
              color: kAccentColor,
            ),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                CircularProgressIndicator(
                  valueColor: AlwaysStoppedAnimation<Color>(Colors.white),
                )
              ],
            ),
          );
        },
        errorBuilder: (_, __, ___) {
          return Container(
            color: kAccentColor,
            child: Icon(
              Icons.landscape,
              size: 100,
              color: kPrimaryColor,
            ),
          );
        },
      ),*/
    );
  }
}
