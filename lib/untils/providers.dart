import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:luscious/controller/category/category_bloc.dart';
import 'package:luscious/controller/recent/recent_cubit.dart';
import 'package:luscious/controller/search/search_bloc.dart';
import 'package:luscious/untils/network/endpoints.dart';

List<BlocProvider> blocProviders = [
  BlocProvider<CategoryBloc>(
    create: (c) => CategoryBloc(
      RepositoryProvider.of<Endpoints>(c),
    ),
  ),
  BlocProvider<SearchBloc>(
    create: (c) => SearchBloc(
      RepositoryProvider.of<Endpoints>(c),
    ),
  ),
  BlocProvider<RecentCubit>(
    create: (c) => RecentCubit(),
  ),
];
