import 'package:luscious/ui/screen/about/about_screen.dart';
import 'package:luscious/ui/screen/category/category_screen.dart';
import 'package:luscious/ui/screen/detail/detail_screen.dart';
import 'package:luscious/ui/screen/home/home_screen.dart';

const homeRoute = '/';
const categoryRoute = '/category';
const detailRoute = '/detail';
const aboutRoute = '/about';

var routes = {
  homeRoute: (context) => HomeScreen(),
  categoryRoute: (context) => CategoryScreen(),
  detailRoute: (context) => DetailScreen(),
  aboutRoute: (context) => AboutScreen(),
};
