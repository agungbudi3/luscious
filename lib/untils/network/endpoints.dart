import 'package:dio/dio.dart';
import 'package:luscious/models/category_res.dart';
import 'package:luscious/models/meal_res.dart';
import 'package:luscious/untils/constants.dart';
import 'package:luscious/untils/network/api_result.dart';

class Endpoints {
  Dio _dio;

  Endpoints() {
    _dio = Dio();

    _dio.options.baseUrl = "https://www.themealdb.com/api/json/v1/1/";

    _dio.options.connectTimeout = kConnectionTimeout;
    _dio.options.receiveTimeout = kConnectionReadTimeout;

    _dio.options.headers['content-type'] = 'application/json';
  }

  Future<ApiResult<CategoryRes>> getCategories() async {
    try {
      final response = await _dio.get(
        'categories.php',
      );
      return ApiResult.success(
        data: CategoryRes.fromJson(response.data),
      );
    } catch (e, s) {
      return ApiResult.failure(
        error: e.getDioException(e),
      );
    }
  }

  Future<ApiResult<MealRes>> getMeals({
    String category,
  }) async {
    try {
      final response = await _dio.get(
        'filter.php',
        queryParameters: {"c": category},
      );
      return ApiResult.success(
        data: MealRes.fromJson(response.data),
      );
    } catch (e, s) {
      return ApiResult.failure(
        error: e.getDioException(e),
      );
    }
  }

  Future<ApiResult<MealRes>> getMealDetail({
    String mealId,
  }) async {
    try {
      final response = await _dio.get(
        'lookup.php',
        queryParameters: {"i": mealId},
      );
      return ApiResult.success(
        data: MealRes.fromJson(response.data),
      );
    } catch (e, s) {
      return ApiResult.failure(
        error: e.getDioException(e),
      );
    }
  }

  Future<ApiResult<MealRes>> searchMeals({
    String search,
  }) async {
    try {
      final response = await _dio.get(
        'search.php',
        queryParameters: {"s": search},
      );
      return ApiResult.success(
        data: MealRes.fromJson(response.data),
      );
    } catch (e, s) {
      return ApiResult.failure(
        error: e.getDioException(e),
      );
    }
  }
}
