import 'package:flutter/material.dart';

//COLOR
const kPrimaryColor = Colors.white;
const kAccentColor = Color(0xFFFDB526);
const kGray1Color = Color(0xFFF0F3FA);
const kGray2Color = Color(0xFFF1F4FB);
const kBlack1Color = Color(0xFF333333);
const kBlack2Color = Color(0xFF666666);
const kBlack3Color = Color(0xFF999999);

const kPrimarySwatch = const MaterialColor(
  0xffffffff,
  const <int, Color>{
    50: const Color(0xffe6e6e6), //10%
    100: const Color(0xffcccccc), //20%
    200: const Color(0xffb3b3b3), //30%
    300: const Color(0xff999999), //40%
    400: const Color(0xff808080), //50%
    500: const Color(0xff666666), //60%
    600: const Color(0xff4c4c4c), //70%
    700: const Color(0xff333333), //80%
    800: const Color(0xff191919), //90%
    900: const Color(0xff000000), //100%
  },
);
//VALUE
const kDefaultPadding = 20.0;
const kMaxWidth = 1232.0;
const kConnectionTimeout = 60000;
const kConnectionReadTimeout = 30000;
const kConnectionWriteTimeOut = 30000;

//TEXT STYLE
const kStyleNunitoExtraLight = TextStyle(
  fontFamily: "Nunito",
  fontWeight: FontWeight.w200,
);
const kStyleNunitoLight = TextStyle(
  fontFamily: "Nunito",
  fontWeight: FontWeight.w300,
);
const kStyleNunitoRegular = TextStyle(
  fontFamily: "Nunito",
  fontWeight: FontWeight.w400,
);
const kStyleNunitoSemiBold = TextStyle(
  fontFamily: "Nunito",
  fontWeight: FontWeight.w600,
);
const kStyleNunitoBold = TextStyle(
  fontFamily: "Nunito",
  fontWeight: FontWeight.w700,
);
const kStyleNunitoExtraBold = TextStyle(
  fontFamily: "Nunito",
  fontWeight: FontWeight.w800,
);
const kStyleNunitoBlack = TextStyle(
  fontFamily: "Nunito",
  fontWeight: FontWeight.w900,
);
