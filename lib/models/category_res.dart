import 'package:luscious/models/category.dart';

class CategoryRes {
  List<Category> _categories;

  List<Category> get categories => _categories;

  CategoryRes({
      List<Category> categories}){
    _categories = categories;
}

  CategoryRes.fromJson(dynamic json) {
    if (json["categories"] != null) {
      _categories = [];
      json["categories"].forEach((v) {
        _categories.add(Category.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    if (_categories != null) {
      map["categories"] = _categories.map((v) => v.toJson()).toList();
    }
    return map;
  }

}