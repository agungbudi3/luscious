class Ingredient {
  String _ingredient;
  String _measurement;

  Ingredient(this._ingredient, this._measurement);

  String get ingredient => _ingredient;

  set ingredient(String value) {
    _ingredient = value;
  }

  String get measurement => _measurement;

  set measurement(String value) {
    _measurement = value;
  }
}
