import 'package:luscious/models/meal.dart';

class MealRes {
  List<Meal> _meals;

  List<Meal> get meals => _meals;

  MealRes({List<Meal> meals}) {
    _meals = meals;
  }

  MealRes.fromJson(dynamic json) {
    if (json["meals"] != null) {
      _meals = [];
      json["meals"].forEach((v) {
        _meals.add(Meal.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    if (_meals != null) {
      map["meals"] = _meals.map((v) => v.toJson()).toList();
    }
    return map;
  }
}
